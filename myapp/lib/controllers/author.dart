import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:myapp/models/author.dart';
import 'package:http/http.dart' as http;

class ViewAuthorBloc{

  late List<Author> listAuthors = [];
  Future<String?> getAuthors() async {
    listAuthors= await fetchAllAuthor();
    if (listAuthors == null) {
      return ("Get all author null");
    }
    return null;
  }

  Future<List<Author>> fetchAllAuthor() async {

    final response = await http
        .get(Uri.parse('http://localhost:3000/author'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      final String responseString = response.body;
      // return List<Author>.from(json.decode(response.body));

      var listAuthor = authorFromJson(responseString);
      debugPrint('====>: ${listAuthor.length}');
      return listAuthor;

    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

}

