import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'controllers/author.dart';
import 'models/author.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<List<Author>> futureAuthor;
  late List<Author> futureAuthors;
  ViewAuthorBloc bloc1 = new ViewAuthorBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureAuthor = bloc1.fetchAllAuthor();
    setState(() {
      futureAuthors = bloc1.listAuthors;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 2000,
        // decoration: const BoxDecoration(
        //   image: DecorationImage(
        //       image: AssetImage("assets/img/bg1.jpg"), fit: BoxFit.cover),
        // ),
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Container(
                height: 300,
                child: Column(
                  children: [
                    Text(
                      "Hoang Huu Manh",
                      style: TextStyle(fontSize: 30),
                    ),
                    Spacer(),
                    Text(
                      "Hoàng Hữu Mạnh",
                      style: TextStyle(fontSize: 40),
                    ),
                    Spacer(),
                    Container(
                      height: 200,
                      color: Colors.indigo,
                      child: FutureBuilder(
                          future: futureAuthor,
                          builder:
                              (context, AsyncSnapshot<List<Author>> snapshot) {
                            if (snapshot.hasData) {
                              List<Author> listAuthor = snapshot.data??[];
                              return Container(
                                  child: ListView.builder(
                                      itemCount: listAuthor.length,
                                      scrollDirection: Axis.vertical,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        // print(listAuthor);
                                        return Column(
                                          children: [
                                            Row(
                                              children: [
                                                SizedBox(width: 50,),
                                                Center(
                                                    child: Text(
                                                      '${listAuthor[index].id}',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          color: Colors.red),
                                                    )),
                                                SizedBox(width: 20,),
                                                Center(
                                                    child: Text(
                                                      '${listAuthor[index].name}',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          color: Colors.red),
                                                    )),
                                                SizedBox(width: 20,),
                                                Center(
                                                    child: Text(
                                                      '${listAuthor[index].year}',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          color: Colors.red),
                                                    )),
                                                SizedBox(width: 20,),
                                                Center(
                                                    child: Text(
                                                      '${listAuthor[index].nickname}',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          color: Colors.red),
                                                    )),

                                              ],
                                            ),
                                            SizedBox(
                                              height: 20,
                                            )
                                          ],
                                        );
                                      }));
                            } else if (snapshot.hasError) {
                              return Text('err data ${snapshot.error}');
                            }

                            // By default, show a loading spinner.
                            return const CircularProgressIndicator();
                          }),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              // Padding(
              //   padding: const EdgeInsets.all(50.0),
              //   child: Table(
              //     border: TableBorder.all(),
              //     defaultVerticalAlignment: TableCellVerticalAlignment.top,
              //     columnWidths: const <int, TableColumnWidth>{
              //       0: FixedColumnWidth(100.0),
              //       1: FixedColumnWidth(100.0),
              //       2: FixedColumnWidth(100.0),
              //     },
              //     children: [
              //       TableRow(children: [
              //         Container(
              //           height: 100,
              //           color: Colors.red,
              //           child: Center(child: Text("1")),
              //         ),
              //         Container(
              //           height: 20,
              //           color: Colors.white,
              //           child: Center(child: Text("2")),
              //         ),
              //         Container(
              //           height: 40,
              //           color: Colors.yellow,
              //           child: Center(child: Text("3")),
              //         ),
              //       ]),
              //       TableRow(children: [
              //         Container(
              //           height: 40,
              //           color: Colors.blueAccent,
              //           child: Center(child: Text("4")),
              //         ),
              //         Container(
              //           height: 80,
              //           color: Colors.green,
              //           child: Center(child: Text("5")),
              //         ),
              //         Container(
              //           height: 10,
              //           color: Colors.purple,
              //           child: Center(child: Text("6")),
              //         ),
              //       ]),
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
