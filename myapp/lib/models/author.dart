// To parse this JSON data, do
//
//     final author = authorFromJson(jsonString);

import 'dart:convert';

List<Author> authorFromJson(String str) => List<Author>.from(json.decode(str).map((x) => Author.fromJson(x)));

String authorToJson(List<Author> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Author {
  Author({
    required this.id,
    required this.name,
    required this.year,
    required this.nickname,
  });

  String id;
  String name;
  String year;
  String nickname;

  factory Author.fromJson(Map<String, dynamic> json) => Author(
    id: json["_id"],
    name: json["name"],
    year: json["year"],
    nickname: json["nickname"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "year": year,
    "nickname": nickname,
  };
}
